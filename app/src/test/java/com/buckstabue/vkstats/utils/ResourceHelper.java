package com.buckstabue.vkstats.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ResourceHelper {
    private ResourceHelper() {
        //no instance
    }

    public static String loadTextFile(TestFile testFile) {
        InputStream inputStream = ResourceHelper.class.getClassLoader().getResourceAsStream(testFile.getName());
        try {
            return streamToString(inputStream, testFile.getEncoding());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String streamToString(InputStream inputStream, String encoding) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        return result.toString(encoding);
    }

    public enum TestFile {
        TEST_RESPONSE("test_response.html", Encoding.CP1251),
        TEST_RESPONSE_EXPECTED_VISITORS_JSON("test_response_expected_visitors.json", Encoding.UTF_8),
        SIMPLE_VALID_RESPONSE("simple_valid_response.html", Encoding.CP1251),
        AUTH_CODE_REQUESTED("auth_code_requested.html", Encoding.UTF_8),
        LOGIN("login.html", Encoding.UTF_8);

        private final String name;
        private final String encoding;

        TestFile(String name, String encoding) {
            this.name = name;
            this.encoding = encoding;
        }

        public String getName() {
            return name;
        }

        public String getEncoding() {
            return encoding;
        }
    }

    private static final class Encoding {
        static final String UTF_8 = "UTF-8";
        static final String CP1251 = "windows-1251";
    }
}
