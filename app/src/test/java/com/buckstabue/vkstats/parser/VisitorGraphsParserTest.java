package com.buckstabue.vkstats.parser;

import com.buckstabue.vkstats.BaseRobolectricTest;
import com.buckstabue.vkstats.exception.ParsingException;
import com.buckstabue.vkstats.utils.ResourceHelper;
import com.buckstabue.vkstats.utils.ResourceHelper.TestFile;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class VisitorGraphsParserTest extends BaseRobolectricTest {
    private static final float EPS = 1e-8f;

    private final VisitorsGraphParser parser = new VisitorsGraphParser();
    private final String VALID_HTML = ResourceHelper.loadTextFile(TestFile.TEST_RESPONSE);
    private final String SIMPLE_VALID_HTML = ResourceHelper.loadTextFile(TestFile.SIMPLE_VALID_RESPONSE);

    @Test
    public void shouldBeAbleToExtractJsonWhenDataIsCorrect() {
        // Given
        String expectedJson = ResourceHelper.loadTextFile(TestFile.TEST_RESPONSE_EXPECTED_VISITORS_JSON
        );

        // When
        String json = VisitorsGraphParser.Companion.extractJson(VALID_HTML);

        // Then
        assertEquals(expectedJson, json);
    }

    @Test(expected = ParsingException.class)
    public void shouldThrowExceptionWhenCouldNotExtractJson() {
        // Given
        String INVALID_HTML = "jfdlsjf;sldjf;dsajflsjadisuvopdsv";

        // When
        parser.parse(INVALID_HTML);
    }

    @Test
    public void shouldReturnCorrectCountOfGroupsWhenInputIsCorrect() {
        // Given

        // When
        VisitorsGraphParser.Result result = parser.parse(SIMPLE_VALID_HTML);

        // Then
        assertNotNull(result);
        LineData perDayLineData = result.getPerDayLineData();
        assertNotNull(perDayLineData);
        LineData perMonthLineData = result.getPerMonthLineData();
        assertNotNull(perMonthLineData);
        assertEquals(2, perDayLineData.getDataSetCount());
        assertEquals(2, perMonthLineData.getDataSetCount());
    }

    @Test
    public void shouldReturnCorrectPerMonthVisitorsWhenInputIsCorrect() {
        // Given
        final Long expectedTimestamp1 = 1479157200l * 1000;
        final Long expectedTimestamp2 = 1481749200l * 1000;
        final int expectedVisitors1 = 120;
        final int expectedVisitors2 = 23;

        // When
        VisitorsGraphParser.Result result = parser.parse(SIMPLE_VALID_HTML);

        // Then
        ILineDataSet perMonthVisitorsDataSet =
                result.getPerMonthLineData().getDataSetByIndex(VisitorsGraphParser.Companion.getVISITORS_INDEX());
        assertEquals(2, perMonthVisitorsDataSet.getEntryCount());

        Entry firstEntry = perMonthVisitorsDataSet.getEntryForIndex(0);
        assertEquals(0, firstEntry.getX(), EPS);
        assertEquals(expectedVisitors1, firstEntry.getY(), EPS);
        assertEquals(expectedTimestamp1, firstEntry.getData());

        Entry secondEntry = perMonthVisitorsDataSet.getEntryForIndex(1);
        assertEquals(1, secondEntry.getX(), EPS);
        assertEquals(expectedVisitors2, secondEntry.getY(), EPS);
        assertEquals(expectedTimestamp2, secondEntry.getData());
    }

    @Test
    public void shouldReturnCorrectPerMonthViewersWhenInputIsCorrect() {
        // Given
        final Long expectedTimestamp1 = 1479157200l * 1000;
        final Long expectedTimestamp2 = 1481749200l * 1000;
        final int expectedViewers1 = 390;
        final int expectedViewers2 = 79;

        // When
        VisitorsGraphParser.Result result = parser.parse(SIMPLE_VALID_HTML);

        // Then
        ILineDataSet perMonthViewersDataSet =
                result.getPerMonthLineData().getDataSetByIndex(VisitorsGraphParser.Companion.getVIEWERS_INDEX());
        assertEquals(2, perMonthViewersDataSet.getEntryCount());

        Entry firstEntry = perMonthViewersDataSet.getEntryForIndex(0);
        assertEquals(0, firstEntry.getX(), EPS);
        assertEquals(expectedViewers1, firstEntry.getY(), EPS);
        assertEquals(expectedTimestamp1, firstEntry.getData());

        Entry secondEntry = perMonthViewersDataSet.getEntryForIndex(1);
        assertEquals(1, secondEntry.getX(), EPS);
        assertEquals(expectedViewers2, secondEntry.getY(), EPS);
        assertEquals(expectedTimestamp2, secondEntry.getData());
    }

    @Test
    public void shouldReturnCorrectPerDayVisitorsWhenInputIsCorrect() {
        // Given
        final Long expectedTimestamp1 = 1481878800l * 1000;
        final Long expectedTimestamp2 = 1481792400l * 1000;
        final int expectedVisitors1 = 3;
        final int expectedVisitors2 = 2;

        // When
        VisitorsGraphParser.Result result = parser.parse(SIMPLE_VALID_HTML);

        // Then
        ILineDataSet perDayVisitorsDataSet =
                result.getPerDayLineData().getDataSetByIndex(VisitorsGraphParser.Companion.getVISITORS_INDEX());
        assertEquals(2, perDayVisitorsDataSet.getEntryCount());

        assertEquals(0, perDayVisitorsDataSet.getEntryForIndex(0).getX(), EPS);
        assertEquals(expectedVisitors2, perDayVisitorsDataSet.getEntryForIndex(0).getY(), EPS);
        assertEquals(expectedTimestamp2, perDayVisitorsDataSet.getEntryForIndex(0).getData());

        assertEquals(1, perDayVisitorsDataSet.getEntryForIndex(1).getX(), EPS);
        assertEquals(expectedVisitors1, perDayVisitorsDataSet.getEntryForIndex(1).getY(), EPS);
        assertEquals(expectedTimestamp1, perDayVisitorsDataSet.getEntryForIndex(1).getData());
    }

    @Test
    public void shouldReturnCorrectPerDayViewersWhenInputIsCorrect() {
        // Given
        final Long expectedTimestamp1 = 1481792400l * 1000;
        final Long expectedTimestamp2 = 1481878800l * 1000;
        final int expectedViewers1 = 3;
        final int expectedViewers2 = 4;

        // When
        VisitorsGraphParser.Result result = parser.parse(SIMPLE_VALID_HTML);

        // Then
        ILineDataSet perDayViewersDataSet =
                result.getPerDayLineData().getDataSetByIndex(VisitorsGraphParser.Companion.getVIEWERS_INDEX());
        assertEquals(2, perDayViewersDataSet.getEntryCount());

        Entry firstEntry = perDayViewersDataSet.getEntryForIndex(0);
        assertEquals(0, firstEntry.getX(), EPS);
        assertEquals(expectedViewers1, firstEntry.getY(), EPS);
        assertEquals(expectedTimestamp1, firstEntry.getData());

        Entry secondEntry = perDayViewersDataSet.getEntryForIndex(1);
        assertEquals(1, secondEntry.getX(), EPS);
        assertEquals(expectedViewers2, secondEntry.getY(), EPS);
        assertEquals(expectedTimestamp2, secondEntry.getData());
    }

}
