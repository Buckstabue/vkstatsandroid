package com.buckstabue.vkstats.manager;

import com.buckstabue.vkstats.exception.PhoneProtectDetectedException;
import com.buckstabue.vkstats.utils.ResourceHelper;
import com.buckstabue.vkstats.utils.ResourceHelper.TestFile;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertSame;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;

@RunWith(BlockJUnit4ClassRunner.class)
public class LoginInteractorTest {

    @Mock
    private HttpClient httpClientMock;

    private LoginInteractor loginInteractor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        loginInteractor = new LoginInteractor(httpClientMock);
    }

    @Test
    public void shouldPhoneProtectExceptionWhenAuthCodeRequested() throws Exception {
        // Given
        String loginHtml = ResourceHelper.loadTextFile(TestFile.LOGIN);
        doReturn(loginHtml).when(httpClientMock).get(eq(LoginInteractor.Companion.getMOBILE_LOGIN_URL()));
        String loginResponseHtml = ResourceHelper.loadTextFile(TestFile.AUTH_CODE_REQUESTED);
        doReturn(loginResponseHtml).when(httpClientMock).post(anyString(), anyMap());

        // When
        PhoneProtectDetectedException expectedException;
        try {
            loginInteractor.execute("login", "password");
            Assert.fail("PhoneProtectDetectedException was not thrown");
            return;
        } catch (PhoneProtectDetectedException e) {
            expectedException = e;
        }

        assertNotNull(expectedException);
        assertSame(loginResponseHtml, expectedException.getHtml());
    }

}
