package com.buckstabue.vkstats.presenter;

import com.buckstabue.vkstats.manager.AppPreferences;
import com.buckstabue.vkstats.ui.view.IntroView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.doReturn;


public class IntroPresenterTest extends BasePresenterTest {
    private IntroPresenter introPresenter;

    @Mock
    private AppPreferences appPreferencesMock;

    @Mock
    private IntroView introViewMock;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        introPresenter = new IntroPresenter(getContext(), appSchedulers, appPreferencesMock);
    }

    @Test
    public void shouldShowLoginScreenWhenNoToken() {
        // Given
        doReturn(null).when(appPreferencesMock).getToken();

        // When
        introPresenter.takeView(introViewMock);

        // Then
        Mockito.verify(introViewMock).showLoginScreen();
    }

    @Test
    public void shouldShowStatsScreenWhenTokenIsAvailable() {
        // Given
        doReturn("non-null-token").when(appPreferencesMock).getToken();

        // When
        introPresenter.takeView(introViewMock);

        // Then
        Mockito.verify(introViewMock).showStatsScreen();
    }
}
