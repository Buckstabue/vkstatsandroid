package com.buckstabue.vkstats.presenter;


import android.content.Context;

import com.buckstabue.vkstats.BaseRobolectricTest;
import com.buckstabue.vkstats.manager.AppSchedulers;

import org.robolectric.RuntimeEnvironment;

import rx.schedulers.Schedulers;


public abstract class BasePresenterTest extends BaseRobolectricTest {
    @SuppressWarnings("WeakerAccess")
    protected final AppSchedulers appSchedulers = new AppSchedulers(Schedulers.immediate(), Schedulers.immediate());

    @SuppressWarnings("WeakerAccess")
    protected final Context getContext() {
        return RuntimeEnvironment.application;
    }
}
