package com.buckstabue.vkstats.presenter;

import com.buckstabue.vkstats.manager.ApiInterface;
import com.buckstabue.vkstats.manager.AppPreferences;
import com.buckstabue.vkstats.parser.CommonChartParser;
import com.buckstabue.vkstats.parser.VisitorsGraphParser;
import com.buckstabue.vkstats.ui.view.StatsView;
import com.buckstabue.vkstats.utils.ResourceHelper;
import com.buckstabue.vkstats.utils.ResourceHelper.TestFile;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Observable;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.notNull;
import static org.mockito.Mockito.verify;

public class StatsPresenterTest extends BasePresenterTest {
    @Mock
    private AppPreferences appPreferencesMock;

    @Mock
    private StatsView statsViewMock;

    @Mock
    private ApiInterface apiInterfaceMock;

    private StatsPresenter statsPresenter;
    private CommonChartParser commonChartParser;
    private final String SIMPLE_VALID_HTML = ResourceHelper.loadTextFile(TestFile.SIMPLE_VALID_RESPONSE);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        VisitorsGraphParser visitorsGraphParser = new VisitorsGraphParser();
        commonChartParser = new CommonChartParser(visitorsGraphParser);
        statsPresenter = new StatsPresenter(getContext(),
                appSchedulers,
                appPreferencesMock,
                apiInterfaceMock,
                commonChartParser);
    }

    @Test
    public void shouldShowPerMonthVisitors() {
        // Given
        doReturn(Observable.just(SIMPLE_VALID_HTML)).when(apiInterfaceMock).loadStatsPage();

        // When
        statsPresenter.takeView(statsViewMock);

        // Then
        verify(statsViewMock).showPerMonthVisitorsLineChart(notNull());
    }

    @Test
    public void shouldShowVisitorsPerDayVisitors() {
        // Given
        doReturn(Observable.just(SIMPLE_VALID_HTML)).when(apiInterfaceMock).loadStatsPage();

        // When
        statsPresenter.takeView(statsViewMock);

        // Then
        verify(statsViewMock).showPerDayVisitorsLineChart(notNull());
    }
}
