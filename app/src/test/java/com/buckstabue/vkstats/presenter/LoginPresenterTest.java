package com.buckstabue.vkstats.presenter;

import com.buckstabue.vkstats.exception.WrongCredentialsException;
import com.buckstabue.vkstats.manager.ApiInterface;
import com.buckstabue.vkstats.ui.view.LoginView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Observable;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

public class LoginPresenterTest extends BasePresenterTest {
    private static final String VALID_LOGIN = "fateslav";
    private static final String VALID_PASSWORD = "qwerty";

    @Mock
    private LoginView loginViewMock;

    @Mock
    private ApiInterface apiInterfaceMock;

    private LoginPresenter loginPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        loginPresenter = new LoginPresenter(getContext(), appSchedulers, apiInterfaceMock);
        loginPresenter.takeView(loginViewMock);
    }

    @Test
    public void shouldShowErrorWhenLoginIsEmpty() {
        // Given
        doReturn("").when(loginViewMock).getLogin();
        doReturn(VALID_PASSWORD).when(loginViewMock).getPassword();

        // When
        loginPresenter.onSignInClicked();

        // Then
        verify(loginViewMock).showLoginValidationError("Логин не может быть пустым.");
    }

    @Test
    public void shouldShowErrorWhenPasswordIsEmpty() {
        // Given
        doReturn(VALID_LOGIN).when(loginViewMock).getLogin();
        doReturn("").when(loginViewMock).getPassword();

        // When
        loginPresenter.onSignInClicked();

        // Then
        verify(loginViewMock).showPasswordValidationError("Пароль не может быть пустым.");
    }

    @Test
    public void shouldShowErrorsWhenBothLoginAndPasswordAreEmpty() {
        // Given
        doReturn("").when(loginViewMock).getLogin();
        doReturn("").when(loginViewMock).getPassword();

        // When
        loginPresenter.onSignInClicked();

        // Then
        verify(loginViewMock).showPasswordValidationError("Пароль не может быть пустым.");
        verify(loginViewMock).showLoginValidationError("Логин не может быть пустым.");
    }

    @Test
    public void shouldNotTryToLoginWhenValidationErrorsExist() {
        // Given
        doReturn("").when(loginViewMock).getLogin();
        doReturn("").when(loginViewMock).getPassword();

        // When
        loginPresenter.onSignInClicked();

        // Then
        verifyZeroInteractions(apiInterfaceMock);
    }

    @Test
    public void shouldShowErrorWhenCredentialsAreWrong() {
        // Given
        doReturn(VALID_LOGIN).when(loginViewMock).getLogin();
        doReturn(VALID_PASSWORD).when(loginViewMock).getPassword();
        doReturn(rx.Observable.error(new WrongCredentialsException()))
                .when(apiInterfaceMock)
                .signIn(anyString(), anyString(), anyBoolean());

        // When
        loginPresenter.onSignInClicked();

        // Then
        verify(loginViewMock).showInvalidCredentialsError();
    }

    @Test
    public void shouldShowStatsScreenWhenCredentialsAreCorrect() {
        // Given
        final boolean rememberMe = true;
        doReturn(VALID_LOGIN).when(loginViewMock).getLogin();
        doReturn(VALID_PASSWORD).when(loginViewMock).getPassword();
        doReturn(rememberMe).when(loginViewMock).isRememberMeChecked();
        doReturn(Observable.just(null))
                .when(apiInterfaceMock)
                .signIn(eq(VALID_LOGIN), eq(VALID_PASSWORD), eq(rememberMe));

        // When
        loginPresenter.onSignInClicked();

        // Then
        verify(loginViewMock).showStatsScreen();
    }

    @Test
    public void shouldHideLoginErrorWhenErrorResolved() {
        // Given
        doReturn("").when(loginViewMock).getLogin();
        doReturn("").when(loginViewMock).getPassword();
        loginPresenter.onSignInClicked();
        doReturn(VALID_LOGIN).when(loginViewMock).getLogin();

        // When
        loginPresenter.onSignInClicked();

        // Then
        verify(loginViewMock).hideLoginValidationError();
    }

    @Test
    public void shouldHidePasswordErrorWhenErrorResolved() {
        // Given
        doReturn("").when(loginViewMock).getLogin();
        doReturn("").when(loginViewMock).getPassword();
        loginPresenter.onSignInClicked();
        doReturn(VALID_PASSWORD).when(loginViewMock).getPassword();

        // When
        loginPresenter.onSignInClicked();

        // Then
        verify(loginViewMock).hidePasswordValidationError();
    }
}
