package com.buckstabue.vkstats.presenter

import android.content.Context

import com.buckstabue.vkstats.manager.AppPreferences
import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.ui.view.IntroView

class IntroPresenter(applicationContext: Context, appSchedulers: AppSchedulers, private val appPreferences: AppPreferences) : BasePresenter<IntroView>(applicationContext, appSchedulers) {

    protected fun onTakeView(introView: IntroView) {
        super.onTakeView(introView)
        if (appPreferences.token == null) {
            view!!.showLoginScreen()
        } else {
            view!!.showStatsScreen()
        }
    }
}
