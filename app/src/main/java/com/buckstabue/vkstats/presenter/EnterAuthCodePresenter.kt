package com.buckstabue.vkstats.presenter

import android.content.Context

import com.buckstabue.vkstats.manager.ApiInterface
import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.ui.view.EnterAuthCodeView

class EnterAuthCodePresenter(applicationContext: Context,
                             appSchedulers: AppSchedulers,
                             private val apiInterface: ApiInterface,
                             initAuthCodePage: String?) : BasePresenter<EnterAuthCodeView>(applicationContext, appSchedulers) {
    private val authCodePage: String? = null

    init {
        var initAuthCodePage = initAuthCodePage
        initAuthCodePage = authCodePage
    }
}
