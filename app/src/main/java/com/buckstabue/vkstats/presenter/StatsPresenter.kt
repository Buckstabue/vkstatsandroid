package com.buckstabue.vkstats.presenter

import android.content.Context

import com.buckstabue.vkstats.manager.ApiInterface
import com.buckstabue.vkstats.manager.AppPreferences
import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.parser.CommonChartParser
import com.buckstabue.vkstats.parser.CommonChartParser.ChartGraphResult
import com.buckstabue.vkstats.ui.view.StatsView
import com.github.mikephil.charting.data.LineData

import rx.Observable

class StatsPresenter(applicationContext: Context,
                     appSchedulers: AppSchedulers,
                     private val appPreferences: AppPreferences,
                     private val apiInterface: ApiInterface,
                     private val commonChartParser: CommonChartParser) : BasePresenter<StatsView>(applicationContext, appSchedulers) {

    protected fun onTakeView(statsView: StatsView) {
        super.onTakeView(statsView)
        executeRequest(createLoadAllChartsObservable(), StatsPageSubscriber())
    }

    private fun createLoadAllChartsObservable(): Observable<List<ChartGraphResult>> {
        return apiInterface.loadStatsPage()
                .flatMap { html -> commonChartParser.getCharts(html) }
                .toList()
    }

    private inner class StatsPageSubscriber : BasePresenter.SimpleSubscriber<List<ChartGraphResult>>() {
        override fun onCompleted() {}

        override fun onNext(graphResults: List<ChartGraphResult>) {
            for (graphResult in graphResults) {
                when (graphResult.chartType) {
                    CommonChartParser.ChartGraphType.VISITORS_PER_MONTH -> view!!.showPerMonthVisitorsLineChart(graphResult.chartData as LineData)
                    CommonChartParser.ChartGraphType.VISITORS_PER_DAY -> view!!.showPerDayVisitorsLineChart(graphResult.chartData as LineData)
                }
            }
        }
    }
}
