package com.buckstabue.vkstats.presenter

import android.content.Context

import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.ui.view.BaseView

import nucleus.presenter.RxPresenter
import nucleus.presenter.delivery.Delivery
import rx.Observable
import rx.Subscriber

abstract class BasePresenter<V : BaseView>(protected val context: Context, private val appSchedulers: AppSchedulers) : RxPresenter<V>() {

    val view: V?
        get() = super.getView()

    protected fun <T> executeRequest(observable: Observable<T>, subscriber: Subscriber<T>) {
        val subscriberWrapper = DeliveryWrapperSubscriber<V, T>(subscriber)
        observable.observeOn(appSchedulers.mainThread())
                .subscribeOn(appSchedulers.io())
                .compose(this.deliverFirst())
                .subscribe(subscriberWrapper)
    }

    private class DeliveryWrapperSubscriber<V, T> internal constructor(private val subscriber: Subscriber<T>) : Subscriber<Delivery<V, T>>() {

        override fun onStart() {
            subscriber.onStart()
        }

        override fun onCompleted() {
            subscriber.onCompleted()
        }

        override fun onError(e: Throwable) {
            subscriber.onError(e)
        }

        override fun onNext(delivery: Delivery<V, T>) {
            delivery.split({ v, t -> subscriber.onNext(t) }, { v, throwable -> subscriber.onError(throwable) })
        }
    }

    protected abstract class SimpleSubscriber<T> : Subscriber<T>() {
        override fun onError(e: Throwable) {
            e.printStackTrace()
        }
    }
}
