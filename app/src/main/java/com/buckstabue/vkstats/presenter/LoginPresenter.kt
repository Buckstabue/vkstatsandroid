package com.buckstabue.vkstats.presenter

import android.content.Context

import com.buckstabue.vkstats.R
import com.buckstabue.vkstats.exception.ValidationException
import com.buckstabue.vkstats.manager.ApiInterface
import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.ui.view.LoginView

class LoginPresenter(applicationContext: Context, appSchedulers: AppSchedulers, private val apiInterface: ApiInterface) : BasePresenter<LoginView>(applicationContext, appSchedulers) {

    fun onSignInClicked(login: String, password: String, rememberMe: Boolean) {
        if (!validateInput(login, password)) {
            return
        }
        executeRequest(apiInterface.signIn(login, password, rememberMe), SignInSubscriber())
    }

    private fun validateInput(login: String, password: String): Boolean {
        var isValid = true
        try {
            validateLogin(login)
            view!!.hideLoginValidationError()
        } catch (e: ValidationException) {
            view!!.showLoginValidationError(e.localizedMessage)
            isValid = false
        }

        try {
            validatePassword(password)
            view!!.hidePasswordValidationError()
        } catch (e: ValidationException) {
            view!!.showPasswordValidationError(e.localizedMessage)
            isValid = false
        }

        return isValid

    }

    @Throws(ValidationException::class)
    private fun validateLogin(login: String) {
        if (login.trim { it <= ' ' }.isEmpty()) {
            throw ValidationException(context.getString(R.string.empty_login_error))
        }
    }

    @Throws(ValidationException::class)
    private fun validatePassword(password: String) {
        if (password.trim { it <= ' ' }.isEmpty()) {
            throw ValidationException(context.getString(R.string.empty_password_error))
        }
    }

    private inner class SignInSubscriber : BasePresenter.SimpleSubscriber<Void>() {
        override fun onCompleted() {
            view!!.showStatsScreen()
        }

        override fun onError(e: Throwable) {
            super.onError(e)
            view!!.showInvalidCredentialsError()
        }

        override fun onNext(aVoid: Void) {

        }
    }
}
