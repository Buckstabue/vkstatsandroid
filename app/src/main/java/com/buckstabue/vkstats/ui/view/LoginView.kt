package com.buckstabue.vkstats.ui.view


interface LoginView : BaseView {
    fun showInvalidCredentialsError()

    fun hideInvalidCredentialsError()

    fun setLogin(login: String)

    fun setPassword(password: String)

    fun setRememberMeChecked(checked: Boolean)

    fun showLoginValidationError(message: String)

    fun hideLoginValidationError()

    fun showPasswordValidationError(message: String)

    fun showStatsScreen()

    fun hidePasswordValidationError()
}
