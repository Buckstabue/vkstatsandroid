package com.buckstabue.vkstats.ui.activity

import android.os.Bundle
import com.buckstabue.vkstats.App
import com.buckstabue.vkstats.R
import com.buckstabue.vkstats.di.component.DaggerIntroComponent
import com.buckstabue.vkstats.di.module.IntroModule
import com.buckstabue.vkstats.presenter.IntroPresenter
import com.buckstabue.vkstats.ui.view.IntroView
import javax.inject.Inject

class IntroActivity : BaseActivity(), IntroView {

    @Inject
    var presenter: IntroPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        val introComponent = DaggerIntroComponent.builder()
                .appComponent(App.getAppComponent())
                .introModule(IntroModule())
                .build()
        introComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
    }

    override fun showLoginScreen() {
        val intent = LoginActivity.getStartIntent(this)
        startActivity(intent)
    }

    override fun showStatsScreen() {
        val intent = StatsActivity.getStartIntent(this)
        startActivity(intent)
    }
}
