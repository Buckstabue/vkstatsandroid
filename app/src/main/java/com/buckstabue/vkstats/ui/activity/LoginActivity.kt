package com.buckstabue.vkstats.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.widget.CheckBox
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.buckstabue.vkstats.App
import com.buckstabue.vkstats.R
import com.buckstabue.vkstats.di.component.DaggerLoginComponent
import com.buckstabue.vkstats.di.module.LoginModule
import com.buckstabue.vkstats.presenter.LoginPresenter
import com.buckstabue.vkstats.ui.view.LoginView
import javax.inject.Inject


class LoginActivity : BaseActivity(), LoginView {
    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    @BindView(R.id.loginInputLayout)
    var loginInputLayout: TextInputLayout? = null

    @BindView(R.id.login)
    var loginEditText: EditText? = null

    @BindView(R.id.passwordInputLayout)
    var passwordInputLayout: TextInputLayout? = null

    @BindView(R.id.password)
    var passwordEditText: EditText? = null

    @BindView(R.id.rememberMe)
    var rememberMeCheckBox: CheckBox? = null

    @Inject
    var presenter: LoginPresenter? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        val loginComponent = DaggerLoginComponent.builder()
                .appComponent(App.getAppComponent())
                .loginModule(LoginModule())
                .build()
        loginComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
    }

    @OnClick(R.id.signIn)
    fun onSignInClicked() {
        val login = loginEditText!!.text.toString()
        val password = passwordEditText!!.text.toString()
        val rememberMe = rememberMeCheckBox!!.isChecked
        presenter!!.onSignInClicked(login, password, rememberMe)
    }

    override fun showInvalidCredentialsError() {
        // TODO: 12/15/16
    }

    override fun hideInvalidCredentialsError() {
        // TODO: 12/15/16
    }

    override fun setLogin(login: String) {
        loginEditText!!.setText(login)
    }

    override fun setPassword(password: String) {
        passwordEditText!!.setText(password)
    }

    override fun setRememberMeChecked(checked: Boolean) {
        rememberMeCheckBox!!.isChecked = checked
    }

    override fun showLoginValidationError(message: String) {
        loginInputLayout!!.error = message
    }

    override fun hideLoginValidationError() {
        loginInputLayout!!.error = null
    }

    override fun showPasswordValidationError(message: String) {
        passwordInputLayout!!.error = message
    }

    override fun showStatsScreen() {
        val intent = StatsActivity.getStartIntent(this)
        startActivity(intent)
    }

    override fun hidePasswordValidationError() {
        passwordInputLayout!!.error = null
    }

}
