package com.buckstabue.vkstats.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle

import com.buckstabue.vkstats.presenter.EnterAuthCodePresenter
import com.buckstabue.vkstats.ui.view.EnterAuthCodeView

import javax.inject.Inject


class EnterAuthCodeActivity : BaseActivity(), EnterAuthCodeView {

    @Inject
    lateinit var presenter: EnterAuthCodePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun showCodeError(errorMessage: String) {

    }

    companion object {
        val EXTRA_INIT_PAGE = "initAuthPage"

        fun getStartIntent(context: Context, initAuthPage: String): Intent {
            val intent = Intent(context, EnterAuthCodeActivity::class.java)
            intent.putExtra(EXTRA_INIT_PAGE, initAuthPage)
            return intent
        }
    }
}
