package com.buckstabue.vkstats.ui.view

interface IntroView : BaseView {
    fun showLoginScreen()

    fun showStatsScreen()
}
