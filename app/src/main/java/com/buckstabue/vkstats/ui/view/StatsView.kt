package com.buckstabue.vkstats.ui.view

import com.github.mikephil.charting.data.LineData

interface StatsView : BaseView {
    fun showPerMonthVisitorsLineChart(perMonthLineData: LineData)

    fun showPerDayVisitorsLineChart(perDayLineData: LineData)

    fun showAgeSexBarChart()

    fun showCriticalNetworkError()

    fun showNetworkError()

    fun showGeographyPieChart()
}
