package com.buckstabue.vkstats.ui.view

interface EnterAuthCodeView : BaseView {
    fun showCodeError(errorMessage: String)
}
