package com.buckstabue.vkstats.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import com.buckstabue.vkstats.App
import com.buckstabue.vkstats.R
import com.buckstabue.vkstats.di.component.DaggerStatsComponent
import com.buckstabue.vkstats.di.module.StatsModule
import com.buckstabue.vkstats.parser.VisitorsGraphParser
import com.buckstabue.vkstats.presenter.StatsPresenter
import com.buckstabue.vkstats.ui.view.StatsView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import rx.functions.Func1
import javax.inject.Inject

class StatsActivity : BaseActivity(), StatsView {
    companion object {

        @ColorRes
        private val VISITORS_COLOR = R.color.light_blue

        @ColorRes
        private val VIEWERS_COLOR = R.color.pink

        fun getStartIntent(context: Context): Intent {
            return Intent(context, StatsActivity::class.java)
        }
    }

    @BindView(R.id.perDayVisitorsLineChart)
    lateinit var perDayVisitorsLineChart: LineChart

    @BindView(R.id.perMonthVisitorsLineChart)
    lateinit var perMonthVisitorsLineChart: LineChart

    @Inject
    lateinit var statsPresenter: StatsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        val statsComponent = DaggerStatsComponent.builder()
                .appComponent(App.getAppComponent())
                .statsModule(StatsModule())
                .build()
        statsComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats)
        ButterKnife.bind(this)

        perDayVisitorsLineChart.xAxis.valueFormatter = DateValueFormatter({ aFloat: Float ->
            perDayVisitorsLineChart!!.data
                    .getDataSetByIndex(VisitorsGraphParser.VISITORS_INDEX)
                    .getEntryForIndex(aFloat!!.toInt())
                    .data as Long
        })
        perDayVisitorsLineChart.setMaxVisibleValueCount(10)
        perDayVisitorsLineChart.isKeepPositionOnRotation = true
        perDayVisitorsLineChart.setPinchZoom(true)
        perDayVisitorsLineChart.isDoubleTapToZoomEnabled = false

        perMonthVisitorsLineChart.xAxis.valueFormatter = MonthValueFormatter({ aFloat ->
            perMonthVisitorsLineChart!!.data
                    .getDataSetByIndex(VisitorsGraphParser.VISITORS_INDEX)
                    .getEntryForIndex(aFloat!!.toInt())
                    .data as Long
        })
        perMonthVisitorsLineChart.setMaxVisibleValueCount(3)
        perMonthVisitorsLineChart.isKeepPositionOnRotation = true
        perMonthVisitorsLineChart.setPinchZoom(true)
        perMonthVisitorsLineChart.isDoubleTapToZoomEnabled = false
    }

    override fun showPerMonthVisitorsLineChart(perMonthLineData: LineData) {
        val visitorsDataSet = perMonthLineData.getDataSetByIndex(VisitorsGraphParser.VISITORS_INDEX) as LineDataSet
        visitorsDataSet.label = getString(R.string.visitors)
        visitorsDataSet.color = ContextCompat.getColor(this, VISITORS_COLOR)

        val viewersDataSet = perMonthLineData.getDataSetByIndex(VisitorsGraphParser.VIEWERS_INDEX) as LineDataSet
        viewersDataSet.label = getString(R.string.views)
        viewersDataSet.color = ContextCompat.getColor(this, VIEWERS_COLOR)

        perMonthVisitorsLineChart.data = perMonthLineData
        perMonthVisitorsLineChart.setVisibleXRangeMaximum(12f)
        perMonthVisitorsLineChart.setVisibleXRangeMinimum(3f)
        perMonthVisitorsLineChart.moveViewToX(Integer.MAX_VALUE.toFloat())
    }

    override fun showPerDayVisitorsLineChart(perDayLineData: LineData) {
        val visitorsDataSet = perDayLineData.getDataSetByIndex(VisitorsGraphParser.VISITORS_INDEX) as LineDataSet
        visitorsDataSet.label = getString(R.string.visitors)
        visitorsDataSet.color = ContextCompat.getColor(this, VISITORS_COLOR)

        val viewersDataSet = perDayLineData.getDataSetByIndex(VisitorsGraphParser.VIEWERS_INDEX) as LineDataSet
        viewersDataSet.label = getString(R.string.views)
        viewersDataSet.color = ContextCompat.getColor(this, VIEWERS_COLOR)

        perDayVisitorsLineChart.data = perDayLineData
        perDayVisitorsLineChart.setVisibleXRangeMaximum(30f)
        perDayVisitorsLineChart.setVisibleXRangeMinimum(3f)
        perDayVisitorsLineChart.moveViewToX(Integer.MAX_VALUE.toFloat())
    }

    override fun showAgeSexBarChart() {

    }

    override fun showCriticalNetworkError() {

    }

    override fun showNetworkError() {

    }

    override fun showGeographyPieChart() {

    }

    private class DateValueFormatter constructor(private val timestampProvider: Func1<Float, Long>) : IAxisValueFormatter {

        override fun getFormattedValue(value: Float, axis: AxisBase): String {
            val timestamp = timestampProvider.call(value)
            return android.text.format.DateFormat.format("d MMMM yy", timestamp!!).toString()
        }
    }

    private class MonthValueFormatter constructor(private val timestampProvider: Func1<Float, Long>) : IAxisValueFormatter {

        override fun getFormattedValue(value: Float, axis: AxisBase): String {
            val timestamp = timestampProvider.call(value)
            return android.text.format.DateFormat.format("MMMM", timestamp!!).toString()
        }
    }
}
