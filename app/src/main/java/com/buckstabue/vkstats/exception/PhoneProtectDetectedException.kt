package com.buckstabue.vkstats.exception

class PhoneProtectDetectedException private constructor(val html: String) : RuntimeException() {
    companion object {

        fun newInstanceWithHtml(html: String): PhoneProtectDetectedException {
            return PhoneProtectDetectedException(html)
        }
    }
}
