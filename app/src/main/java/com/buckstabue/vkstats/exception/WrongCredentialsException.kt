package com.buckstabue.vkstats.exception

class WrongCredentialsException : RuntimeException()
