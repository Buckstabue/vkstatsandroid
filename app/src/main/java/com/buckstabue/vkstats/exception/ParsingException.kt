package com.buckstabue.vkstats.exception

class ParsingException : RuntimeException {
    constructor() {}

    constructor(message: String) : super(message) {}

    constructor(cause: Throwable) : super(cause) {}
}
