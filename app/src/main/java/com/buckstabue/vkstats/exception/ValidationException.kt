package com.buckstabue.vkstats.exception

class ValidationException(message: String) : Exception(message)
