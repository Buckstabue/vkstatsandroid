package com.buckstabue.vkstats

import android.app.Application
import android.content.Context

import com.buckstabue.vkstats.di.component.AppComponent
import com.buckstabue.vkstats.di.component.DaggerAppComponent
import com.buckstabue.vkstats.di.module.AppModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = this
    }

    companion object {
        @Volatile private val appComponent: AppComponent? = null
        private var appContext: Context? = null

        fun getAppComponent(): AppComponent? {
            if (appComponent == null) {
                synchronized(App::class.java) {
                    if (appComponent == null) {
                        return DaggerAppComponent.builder()
                                .appModule(AppModule(appContext))
                                .build()
                    }
                }
            }
            return appComponent
        }
    }
}
