package com.buckstabue.vkstats.manager

import java.io.IOException

interface HttpClient {
    @Throws(IOException::class)
    operator fun get(url: String): String

    @Throws(IOException::class)
    fun post(url: String, postQueryMap: Map<String, String>): String
}
