package com.buckstabue.vkstats.manager

import com.buckstabue.vkstats.exception.PhoneProtectDetectedException

import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import java.io.IOException
import java.util.HashMap

class LoginInteractor(private val httpClient: HttpClient) {

    @Throws(IOException::class, PhoneProtectDetectedException::class)
    fun execute(login: String, password: String) {
        val loginPage = httpClient.get(MOBILE_LOGIN_URL)
        val document = Jsoup.parse(loginPage)
        val formActionUrl = document.body()
                .getElementsByTag("form")
                .first()
                .attr("action")
        val params = HashMap<String, String>()
        params.put(ATTR_LOGIN, login)
        params.put(ATTR_PASSWORD, password)

        val response = httpClient.post(formActionUrl, params)
        if (isAuthCodeRequested(response)) {
            throw PhoneProtectDetectedException.newInstanceWithHtml(response)
        }
    }

    private fun isAuthCodeRequested(html: String): Boolean {
        return html.indexOf("act=authcheck_code") != -1
    }

    companion object {
        internal val MOBILE_LOGIN_URL = "https://m.vk.com/"
        private val ATTR_LOGIN = "email"
        private val ATTR_PASSWORD = "pass"
    }
}
