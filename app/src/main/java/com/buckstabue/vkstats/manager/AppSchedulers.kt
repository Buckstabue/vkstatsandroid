package com.buckstabue.vkstats.manager

import rx.Scheduler

class AppSchedulers(private val mainThreadScheduler: Scheduler, private val ioScheduler: Scheduler) {

    fun mainThread(): Scheduler {
        return mainThreadScheduler
    }

    fun io(): Scheduler {
        return ioScheduler
    }
}
