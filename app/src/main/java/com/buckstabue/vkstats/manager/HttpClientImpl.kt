package com.buckstabue.vkstats.manager

import java.io.IOException

import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request

class HttpClientImpl(private val httpClient: OkHttpClient, private val userAgent: String) : HttpClient {

    @Throws(IOException::class)
    override fun get(url: String): String {
        val requestBuilder = Request.Builder()
        requestBuilder.url(url)
        requestBuilder.addHeader(HEADER_USER_AGENT, userAgent)
        return httpClient.newCall(requestBuilder.build()).execute().body().string()
    }

    @Throws(IOException::class)
    override fun post(url: String, postQueryMap: Map<String, String>): String {
        val requestBuilder = Request.Builder()
        val formBodyBuilder = FormBody.Builder()
        for ((key, value) in postQueryMap) {
            formBodyBuilder.add(key, value)
        }
        val formBody = formBodyBuilder.build()
        val request = requestBuilder.url(url).post(formBody).build()
        return httpClient.newCall(request)
                .execute()
                .body()
                .string()
    }

    companion object {
        private val HEADER_USER_AGENT = "User-agent"
        val MOBILE_USER_AGENT = "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C)" + " AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
        val DESKTOP_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0"
    }
}
