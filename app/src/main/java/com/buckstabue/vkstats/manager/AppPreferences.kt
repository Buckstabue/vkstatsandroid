package com.buckstabue.vkstats.manager

import android.content.Context
import android.content.SharedPreferences

class AppPreferences(applicationContext: Context) {

    private val preferences: SharedPreferences

    init {
        preferences = applicationContext.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
    }

    var token: String?
        get() = preferences.getString(PREF_TOKEN, null)
        set(token) = setStringPref(PREF_TOKEN, token)

    private fun setStringPref(prefName: String, prefValue: String?) {
        preferences.edit()
                .putString(prefName, prefValue)
                .apply()
    }

    companion object {
        private val FILE_NAME = "app_preferences"
        private val PREF_TOKEN = "token"
    }
}
