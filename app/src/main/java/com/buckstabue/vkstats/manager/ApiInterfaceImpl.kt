package com.buckstabue.vkstats.manager

import android.content.Context

import com.buckstabue.vkstats.R

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream

import rx.Observable
import rx.exceptions.Exceptions

class ApiInterfaceImpl(private val context: Context, private val loginInteractor: LoginInteractor) : ApiInterface {

    override fun signIn(login: String, password: String, rememberMe: Boolean): Observable<Void> {
        return Observable.fromCallable {
            loginInteractor.execute(login, password)
            null
        }
    }

    override fun loadStatsPage(): Observable<String> {
        return Observable.fromCallable { context.resources.openRawResource(R.raw.test_response) }
                .map { inputStream ->
                    try {
                        // TODO: 18.12.2016 remove test code
                        return@Observable.fromCallable(() -> context.getResources().openRawResource(R.raw.test_response))
                        .map streamToString inputStream, "windows-1251")
                    } catch (e: IOException) {
                        throw Exceptions.propagate(e)
                    }
                }
    }

    @Throws(IOException::class)
    private fun streamToString(inputStream: InputStream, encoding: String): String {
        // TODO: 18.12.2016 remove test code
        val result = ByteArrayOutputStream()
        val buffer = ByteArray(1024)
        var length: Int
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length)
        }
        return result.toString(encoding)
    }
}
