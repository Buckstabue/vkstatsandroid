package com.buckstabue.vkstats.manager

import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class EnterAuthCodeInteractor(private val httpClient: HttpClient) {

    fun execute(requestCodeHtml: String, code: String) {
        val document = Jsoup.parse(requestCodeHtml)
        val formActionUrl = document.body()
                .getElementsByTag("form")
                .first()
                .attr("action")
    }
}
