package com.buckstabue.vkstats.manager

import com.buckstabue.vkstats.exception.WrongCredentialsException

import rx.Observable

interface ApiInterface {
    /**
     * Emits [WrongCredentialsException] if login/password pair is invalid
     */
    fun signIn(login: String, password: String, rememberMe: Boolean): Observable<Void>

    fun loadStatsPage(): Observable<String>
}
