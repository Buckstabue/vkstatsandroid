package com.buckstabue.vkstats.parser

import com.github.mikephil.charting.data.ChartData

import rx.Observable

class CommonChartParser(private val visitorsGraphParser: VisitorsGraphParser) {

    fun getCharts(html: String): Observable<ChartGraphResult> {
        return createLoadVisitorsChartObservable(html)
    }

    private fun createLoadVisitorsChartObservable(html: String): Observable<ChartGraphResult> {
        return Observable.fromCallable<Result> { visitorsGraphParser.parse(html) }
                .flatMap { result ->
                    Observable.just(
                            ChartGraphResult(ChartGraphType.VISITORS_PER_DAY, result.perDayLineData),
                            ChartGraphResult(ChartGraphType.VISITORS_PER_MONTH, result.perMonthLineData))
                }
    }

    class ChartGraphResult(val chartType: ChartGraphType, val chartData: ChartData<*>)

    enum class ChartGraphType {
        VISITORS_PER_MONTH,
        VISITORS_PER_DAY
    }
}
