package com.buckstabue.vkstats.parser

import com.buckstabue.vkstats.exception.ParsingException
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet

import java.io.IOException
import java.util.ArrayList
import java.util.Collections

class VisitorsGraphParser {

    @Throws(ParsingException::class)
    fun parse(html: String): Result {
        val json = extractJson(html)
        val jsonFactory = JsonFactory()
        val jsonParser: JsonParser
        try {
            jsonParser = jsonFactory.createParser(json)
        } catch (e: IOException) {
            throw ParsingException(e)
        }

        try {
            if (jsonParser.nextToken() != JsonToken.START_ARRAY) {
                throw ParsingException("Expected array token but got " + jsonParser.currentToken().toString())
            }
            val perDayLineData = parseCategoryLineData(jsonParser)
            val perMonthLineData = parseCategoryLineData(jsonParser)
            return Result(perDayLineData, perMonthLineData)
        } catch (e: IOException) {
            throw ParsingException(e)
        }

    }

    // usually there are two categories: amount visitors per day and amount of visitors per month
    @Throws(IOException::class, ParsingException::class)
    private fun parseCategoryLineData(jsonParser: JsonParser): LineData {
        if (jsonParser.nextToken() != JsonToken.START_ARRAY) {
            throw ParsingException("Expected array token but got " + jsonParser.currentToken().toString())
        }
        val uniqueVisitorsDataSet = parseGraphPoints(jsonParser)
        val viewersDataSet = parseGraphPoints(jsonParser)
        skipArray(jsonParser)
        return LineData(uniqueVisitorsDataSet, viewersDataSet)
    }

    @Throws(IOException::class, ParsingException::class)
    private fun parseGraphPoints(jsonParser: JsonParser): ILineDataSet {
        if (jsonParser.nextToken() != JsonToken.START_OBJECT) {
            throw ParsingException("Expected object token but got " + jsonParser.currentToken().toString())
        }
        skipToField("d", jsonParser)
        if (jsonParser.nextToken() != JsonToken.START_ARRAY) {
            throw ParsingException("Expected array token but got " + jsonParser.currentToken().toString())
        }
        // TODO what's the most popular init size?
        val entries = ArrayList<Entry>()
        var entryCounter = 0
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
            ++entryCounter
            val entry = parseSinglePoint(jsonParser, entryCounter)
            entries.add(entry)
        }
        // sort entries by age in the ascending order, by default they are retrieved in the descending order
        Collections.reverse(entries)
        for (entry in entries) {
            entry.x = entryCounter - entry.x
        }
        skipObject(jsonParser)
        return LineDataSet(entries, DEFAULT_LABEL)
    }

    // it parses lines like [1479157200, 120, ""] - [timestamp, numberOfVisitors, blah blah]
    @Throws(IOException::class, ParsingException::class)
    private fun parseSinglePoint(jsonParser: JsonParser, yCoordinate: Int): Entry {
        if (jsonParser.currentToken() != JsonToken.START_ARRAY) {
            throw ParsingException("Expected array token but got " + jsonParser.currentToken().toString())
        }
        if (jsonParser.nextToken() != JsonToken.VALUE_NUMBER_INT) {
            throw ParsingException("Expected float value token but got " + jsonParser.currentToken().toString())
        }
        val timestamp = jsonParser.longValue * 1000
        if (jsonParser.nextToken() != JsonToken.VALUE_NUMBER_INT) {
            throw ParsingException("Expected int value token but got " + jsonParser.currentToken().toString())
        }
        val visitsCount = jsonParser.intValue
        skipArray(jsonParser)
        return Entry(yCoordinate.toFloat(), visitsCount.toFloat(), timestamp)
    }

    @Throws(ParsingException::class, IOException::class)
    private fun skipToField(fieldName: String, jsonParser: JsonParser) {
        while (jsonParser.nextToken() != JsonToken.FIELD_NAME || fieldName != jsonParser.currentName) {
            if (jsonParser.isExpectedStartArrayToken) {
                skipArray(jsonParser)
            }
            if (jsonParser.isExpectedStartObjectToken) {
                skipObject(jsonParser)
            }
        }
        // if we are here and no exception is thrown, then the current token refers to the fieldName parameter
    }

    @Throws(IOException::class)
    private fun skipArray(jsonParser: JsonParser) {
        var nestedArrayDepth = 1
        while (nestedArrayDepth > 0) {
            jsonParser.nextToken()
            if (jsonParser.isExpectedStartArrayToken) {
                ++nestedArrayDepth
            } else if (jsonParser.currentToken() == JsonToken.END_ARRAY) {
                --nestedArrayDepth
            }
        }
    }

    @Throws(IOException::class)
    private fun skipObject(jsonParser: JsonParser) {
        var nestedObjectDepth = 1
        while (nestedObjectDepth > 0) {
            jsonParser.nextToken()
            if (jsonParser.isExpectedStartObjectToken) {
                ++nestedObjectDepth
            } else if (jsonParser.currentToken() == JsonToken.END_OBJECT) {
                --nestedObjectDepth
            }
        }
    }

    class Result(val perDayLineData: LineData, val perMonthLineData: LineData)

    companion object {
        val VISITORS_INDEX = 0
        val VIEWERS_INDEX = 1

        private val DEFAULT_LABEL = "default_label"

        @Throws(ParsingException::class)
        internal fun extractJson(html: String): String {
            val searchedText = "cur.graphDatas['visitors_graph'] = '"
            val pos = html.indexOf(searchedText)
            if (pos == -1) {
                throw ParsingException("searched text is not found")
            }
            val startPos = pos + searchedText.length
            val endPos = html.indexOf('\'', startPos)
            if (endPos == -1) {
                throw ParsingException("searched text is not found")
            }
            return html.substring(startPos, endPos)
        }
    }
}
