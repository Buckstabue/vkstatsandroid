package com.buckstabue.vkstats.di.component

import com.buckstabue.vkstats.di.module.LoginModule
import com.buckstabue.vkstats.di.scope.PerActivity
import com.buckstabue.vkstats.ui.activity.LoginActivity

import dagger.Component

@PerActivity
@Component(modules = arrayOf(LoginModule::class), dependencies = arrayOf(AppComponent::class))
interface LoginComponent {
    fun inject(loginActivity: LoginActivity)
}
