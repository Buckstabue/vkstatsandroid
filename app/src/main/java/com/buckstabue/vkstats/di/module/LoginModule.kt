package com.buckstabue.vkstats.di.module

import android.content.Context

import com.buckstabue.vkstats.di.scope.PerActivity
import com.buckstabue.vkstats.manager.ApiInterface
import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.presenter.LoginPresenter

import dagger.Module
import dagger.Provides

@Module
class LoginModule {

    @Provides
    @PerActivity
    internal fun provideLoginPresenter(context: Context, appSchedulers: AppSchedulers, apiInterface: ApiInterface): LoginPresenter {
        return LoginPresenter(context, appSchedulers, apiInterface)
    }
}
