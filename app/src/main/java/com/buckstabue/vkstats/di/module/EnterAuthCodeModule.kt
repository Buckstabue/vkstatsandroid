package com.buckstabue.vkstats.di.module

import android.content.Context

import com.buckstabue.vkstats.di.scope.PerActivity
import com.buckstabue.vkstats.manager.ApiInterface
import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.presenter.EnterAuthCodePresenter

import dagger.Module
import dagger.Provides

@Module
class EnterAuthCodeModule(private val initAuthCodePage: String) {

    @Provides
    @PerActivity
    internal fun provideEnterAuthCodePresenter(context: Context,
                                               appSchedulers: AppSchedulers,
                                               apiInterface: ApiInterface): EnterAuthCodePresenter {
        return EnterAuthCodePresenter(context, appSchedulers, apiInterface, initAuthCodePage)
    }
}
