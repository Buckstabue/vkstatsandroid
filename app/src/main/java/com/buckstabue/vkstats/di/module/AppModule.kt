package com.buckstabue.vkstats.di.module

import android.content.Context

import com.buckstabue.vkstats.BuildConfig
import com.buckstabue.vkstats.di.scope.PerApplication
import com.buckstabue.vkstats.manager.ApiInterface
import com.buckstabue.vkstats.manager.ApiInterfaceImpl
import com.buckstabue.vkstats.manager.AppPreferences
import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.manager.HttpClient
import com.buckstabue.vkstats.manager.HttpClientImpl
import com.buckstabue.vkstats.manager.LoginInteractor
import com.franmontiel.persistentcookiejar.ClearableCookieJar
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

@Module
class AppModule(private val applicationContext: Context) {

    @Provides
    @PerApplication
    internal fun provideApplicationContext(): Context {
        return this.applicationContext
    }

    @Provides
    @PerApplication
    internal fun provideAppPreferences(context: Context): AppPreferences {
        return AppPreferences(context)
    }

    @Provides
    @PerApplication
    internal fun provideApiInterface(context: Context, loginInteractor: LoginInteractor): ApiInterface {
        return ApiInterfaceImpl(context, loginInteractor)
    }

    @Provides
    @PerApplication
    internal fun provideAppSchedulers(): AppSchedulers {
        return AppSchedulers(AndroidSchedulers.mainThread(), Schedulers.io())
    }

    @Provides
    internal fun provideLoginInteractor(httpClient: HttpClient): LoginInteractor {
        return LoginInteractor(httpClient)
    }

    @Provides
    @PerApplication
    internal fun provideMobileHttpClient(okHttpClient: OkHttpClient): HttpClient {
        return HttpClientImpl(okHttpClient, HttpClientImpl.MOBILE_USER_AGENT)
    }

    @Provides
    @PerApplication
    internal fun provideOkHttpClient(cookieJar: ClearableCookieJar): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
                .setLevel(if (BuildConfig.DEBUG) Level.BODY else Level.NONE)
        return OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .cookieJar(cookieJar)
                .build()
    }

    @Provides
    @PerApplication
    internal fun provideCookieJar(context: Context): ClearableCookieJar {
        return PersistentCookieJar(SetCookieCache(), SharedPrefsCookiePersistor(context))
    }
}
