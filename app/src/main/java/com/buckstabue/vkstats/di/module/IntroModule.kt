package com.buckstabue.vkstats.di.module

import android.content.Context

import com.buckstabue.vkstats.di.scope.PerActivity
import com.buckstabue.vkstats.manager.AppPreferences
import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.presenter.IntroPresenter

import dagger.Module
import dagger.Provides

@Module
class IntroModule {

    @PerActivity
    @Provides
    internal fun provideIntroPresenter(context: Context, appSchedulers: AppSchedulers, appPreferences: AppPreferences): IntroPresenter {
        return IntroPresenter(context, appSchedulers, appPreferences)
    }
}
