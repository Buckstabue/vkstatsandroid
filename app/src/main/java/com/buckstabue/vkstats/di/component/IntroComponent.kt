package com.buckstabue.vkstats.di.component

import com.buckstabue.vkstats.di.module.IntroModule
import com.buckstabue.vkstats.di.scope.PerActivity
import com.buckstabue.vkstats.ui.activity.IntroActivity

import dagger.Component

@PerActivity
@Component(modules = arrayOf(IntroModule::class), dependencies = arrayOf(AppComponent::class))
interface IntroComponent {
    fun inject(introActivity: IntroActivity)
}
