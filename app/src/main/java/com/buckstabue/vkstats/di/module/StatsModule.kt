package com.buckstabue.vkstats.di.module

import android.content.Context

import com.buckstabue.vkstats.di.scope.PerActivity
import com.buckstabue.vkstats.manager.ApiInterface
import com.buckstabue.vkstats.manager.AppPreferences
import com.buckstabue.vkstats.manager.AppSchedulers
import com.buckstabue.vkstats.parser.CommonChartParser
import com.buckstabue.vkstats.parser.VisitorsGraphParser
import com.buckstabue.vkstats.presenter.StatsPresenter

import dagger.Module
import dagger.Provides

@PerActivity
@Module
class StatsModule {

    @Provides
    @PerActivity
    internal fun provideCommonChartParser(visitorsGraphParser: VisitorsGraphParser): CommonChartParser {
        return CommonChartParser(visitorsGraphParser)
    }

    @Provides
    @PerActivity
    internal fun provideStatsPresenter(context: Context,
                                       appSchedulers: AppSchedulers,
                                       appPreferences: AppPreferences,
                                       apiInterface: ApiInterface,
                                       commonChartParser: CommonChartParser): StatsPresenter {
        return StatsPresenter(context, appSchedulers, appPreferences, apiInterface, commonChartParser)
    }

    @Provides
    @PerActivity
    internal fun provideVisitorsGraphParser(): VisitorsGraphParser {
        return VisitorsGraphParser()
    }
}
