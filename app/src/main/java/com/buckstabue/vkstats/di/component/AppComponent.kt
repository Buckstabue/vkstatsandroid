package com.buckstabue.vkstats.di.component

import android.content.Context

import com.buckstabue.vkstats.di.module.AppModule
import com.buckstabue.vkstats.di.scope.PerApplication
import com.buckstabue.vkstats.manager.ApiInterface
import com.buckstabue.vkstats.manager.AppPreferences
import com.buckstabue.vkstats.manager.AppSchedulers

import dagger.Component

@PerApplication
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun context(): Context

    fun appSchedulers(): AppSchedulers

    fun apiInterface(): ApiInterface

    fun appPreferences(): AppPreferences
}
