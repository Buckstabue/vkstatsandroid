package com.buckstabue.vkstats.di.component

import com.buckstabue.vkstats.di.module.StatsModule
import com.buckstabue.vkstats.di.scope.PerActivity
import com.buckstabue.vkstats.ui.activity.StatsActivity

import dagger.Component

@PerActivity
@Component(modules = arrayOf(StatsModule::class), dependencies = arrayOf(AppComponent::class))
interface StatsComponent {
    fun inject(statsActivity: StatsActivity)
}
